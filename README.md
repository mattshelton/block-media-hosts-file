# Block Mediate Sites Hosts File

I collected sets of hostnames from various block lists, as well as DNS replacement lists where DNS blocks are in effect, and wrote a hosts file to block certain video and photo sharing sites. The context for this was within a lab environment in a school where these sites have academic purposes in parts of the network, but not all of it, and we could not use vlan-specific filtering rules.

This is an inelegant way of filtering, but it works in a pinch.
